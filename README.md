1) git clone https://gitlab.com/dervined/wsce.git
2) wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
3) bash Miniconda3*.sh (init conda = false)
4) ввести команду которая отображается после установки (eval "$(* shell.bash hook)"
5) conda init
6) conda deactivate
7) conda update conda
8) conda create --name wsce python=3.7
9) conda activate wsce (conda deactivate)
10) conda config --add channels conda-forge
11) conda install pyopencl
12) conda install pocl oclgrind
13) pip install Django==2.1.7 django-cleanup scipy
14) установить драйвер для видеокарты

Указать на каком процессоре выполнять вычисления:
1) перейти –> apps/lcc/lib/main.py
2) находим строчку "_PU = cl.get_platforms()[0].get_devices()[1]"
3) меняем индекс ('get_devices')