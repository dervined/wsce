from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.generic import View

from apps.lcc.models import DataFileModel
from apps.lcc.lib.main import main, read_wavfile

import json

class Home(View):
    def get(self, request):
        # Data_Files
        data_files = DataFileModel.objects.all().order_by('date_of_creation')
        context = {'title': 'Главная страница',
                    'data_files': data_files}
        return render(request, 'base/pages/home/home.html', context)

def getLength(request):
    if request.method == 'POST':
        # Данные полученные с ajax post
        id_selected = int(request.POST['selected'])
        # Узнать длину
        file_data = DataFileModel.objects.get(id=id_selected)
        samplerate, data = read_wavfile(file_data.datafile.path)
        length_data = '{} – {} сек'.format(str(len(data)), str(round(len(data)/samplerate, 3)))
        print(length_data)
        return HttpResponse(length_data)

def selected_data_list(request):
    if request.method == 'POST':
        # Данные полученные с ajax post
        dataList_selected = request.POST['dataList_selected']
        dataList_selected_processed = list(map(lambda el: int(el), dataList_selected[1:].split(' ')))
        selectRange_0 = int(request.POST['selectRange_0'])
        selectRange_1 = int(request.POST['selectRange_1'])
        selectStep = int(request.POST['selectStep'])
        isSeconds = True if(request.POST['isSeconds']=='True') else False
        print(isSeconds)
        print(dataList_selected_processed)
        data_files = DataFileModel.objects.all().filter(id__in=dataList_selected_processed)
        x = []
        samplerate_arr = []
        # Для автокорреляции
        if(len(data_files)==1):
            data_files = list(data_files)
            data_files.append(data_files[0])
        # продолжение
        for data in data_files:
            samplerate, file_readed = read_wavfile(data.datafile.path)
            x.append(file_readed)
            samplerate_arr.append(samplerate)
        if (samplerate_arr[0] != samplerate_arr[1]):
            return HttpResponse(status=406)
        main(x[0], x[1], selectRange_0, selectRange_1, selectStep,  'tmp/new_textfile.txt', samplerate=samplerate_arr[0], isSeconds=isSeconds)
        res_file = open('tmp/new_textfile.txt')
        res_data = []
        for line in res_file:
            lag,lcc = line.split('\t')
            res_data.append([float(lag),float(lcc)])
        res_file.close()
        json.dumps(res_data)
        return HttpResponse(json.dumps(res_data))