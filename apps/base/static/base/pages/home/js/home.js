// Ограничение выбора файлов по количеству
$('input.max2').on('change', function(event) {
    var limit = 2
    file_id = $(this).val()
    if($(this).siblings(':checked').length >= limit) {
        this.checked = false;
    }
    else{
        $.ajax({
            type: 'POST',
            url: '/getLength/',
            data: {
                selected: file_id,
                csrfmiddlewaretoken: $('.data-list input[name=csrfmiddlewaretoken]').val()
            },
            success: function (data) {
                $('.data-list #file-'+file_id).text(data)
            },
            error: function () {
                alert('Не удалось получить количество значений файла')
            }
        })
    }
});
function getIdByDataListSelected(parent){
    res = ''
    parent.children('input:checked').each(function(index, item){
        res += ' ' + (Number($(item).val()))
    })

    return res
}
$('.data-list button').on('click', function(){
    $('.data-list button').text('Загрузка...')
    $.ajax({
        type: 'POST',
        url: '/selected-data-list/',
        data: {
            dataList_selected: getIdByDataListSelected($('.data-list')),
            selectRange_0: $('.selectRange_0').val(),
            selectRange_1: $('.selectRange_1').val(),
            selectStep: $('.selectStep').val(),
            isSeconds: $('.isSeconds:checked').length>0?'True':'False',
            csrfmiddlewaretoken: $('.data-list input[name=csrfmiddlewaretoken]').val()
        },
        success: function (data) {
            $('.data-list button').text('Completed')
            drawGraph(JSON.parse(data))
        },
        error: function (e) {
            if (e.status === 406) {
                alert('Ошибка! Разная частота дискретизации.')
            }
            else {
                $('.data-list button').text('Произошла ошибка')
            }
        }
    })
})

function drawGraph(array){
    g = new Dygraph(document.getElementById("dygraph"),
    array,
    {
        labels: [ "X", "Y" ]
    });
}

$(document).ready(function() {
    
})