from django.contrib import admin
from django.urls import path, include

from .views import Home, selected_data_list, getLength
# for media download
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.static import static

urlpatterns = [
    path('', Home.as_view(), name='home'),
    path('selected-data-list/', selected_data_list),
    path('getLength/', getLength, name="getLength")
]