from django.db import models
from django.utils import timezone

# Create your models here.

def file_directory_path(instance, filename):
    dateTime = str(timezone.now()).split(' ')
    date = dateTime[0]
    add_path = "{}/{}".format(date, instance.name)
    return 'lcc/data_files/{}/{}'.format(add_path, filename)
    
class DataFileModel(models.Model):
    objects = models.Manager()
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=100, blank=False)
    datafile = models.FileField(upload_to=file_directory_path)
    date_of_creation = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        dateTime = str(self.date_of_creation).split(' ')
        resData = "{} {}".format(dateTime[0], dateTime[1].split('.')[0])
        return "{}_'{}'".format(resData, self.name)