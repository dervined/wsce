# Generated by Django 2.1.7 on 2019-05-19 08:01

import apps.lcc.models
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DataFileModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('datafile', models.FileField(upload_to=apps.lcc.models.file_directory_path)),
                ('date_of_creation', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
