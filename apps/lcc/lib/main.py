#Рабочее название: Оценка корреляционных связей аккустических или *** сигналов с использованием параллельных вычислений

import pyopencl as cl  # Import the OpenCL GPU computing API
import pyopencl.array as cl_array  # Import PyOpenCL Array (a Numpy array plus an OpenCL buffer object)
from pyopencl.reduction import ReductionKernel
from pyopencl.elementwise import ElementwiseKernel
import numpy  # Import Numpy number tools
from math import sqrt
import time
from scipy.io import wavfile
from functools import reduce

#Init:
_PU = cl.get_platforms()[0].get_devices()[1]
print(_PU)
context = cl.Context([_PU])  # Initialize the Context
queue = cl.CommandQueue(context)  # Instantiate a Queue
#kernel functions for work:
sum1 = ReductionKernel(context, numpy.float32, neutral="0",
                        reduce_expr="a+b", map_expr="x[i]",
                        arguments="__global float *x") #reduce(lambda el1,el2:el1+el2,array)
f2 = ElementwiseKernel(context,
                        "float *x, float av, float *c",
                        "c[i] = x[i] - av", "f2") #map(lambda el1: el1-av, array)
f3 = ElementwiseKernel(context,
                        "float *x, float *y, float *c",
                        "c[i] = x[i]*y[i]", "f3") #map(lambda el1,el2: el1*el2, array1, array2)

#basic function:
def compress_arr(arr, samplerate):
    steps = int(len(arr)/samplerate)
    res = []
    for step in range(steps):
        x = arr[step*samplerate: (step+1)*samplerate]
        s = sum1(x).get()
        average = s/samplerate
        res.append(average)
    return numpy.array(res).astype(numpy.float32)

def LCC(array1, array2):
    sx1 = sum1(array1).get()
    x_average = sx1/len(array1) #x_average
    sy1 = sum1(array2).get()
    y_average = sy1/len(array2) #y_average
    x_n1 = cl_array.empty_like(array1) #x_n1 (x-x_average)
    f2(array1, x_average, x_n1)
    y_n1 = cl_array.empty_like(array2) #y_n1 (y-y_average)
    f2(array2, y_average, y_n1) 
    xy_n1 = cl_array.empty_like(array1) #xy_n1
    f3(x_n1, y_n1, xy_n1)
    numerator = sum1(xy_n1).get() #numerator
    xx_n1 = cl_array.empty_like(array1) #xx_n1
    f3(x_n1, x_n1, xx_n1)
    yy_n1 = cl.array.empty_like(array2) #yy_n1
    f3(y_n1, y_n1, yy_n1)
    xx_sum1 = sum1(xx_n1).get() #xx_sum1
    yy_sum1 = sum1(yy_n1).get() #yy_sum1
    xxyy_d1 = xx_sum1 * yy_sum1 #xxyy_d1
    denominator = sqrt(xxyy_d1)
    return numerator/denominator
# Добавить возможность изменять точку старта (отсчета) у arr2_lag (Подробнее фото в телефоне)
def LCCRange_S(pathfile, samplerate, arr1, arr2, selectedRange, step, startArr2_lag = 0, isSeconds=False):    
    arr1_fix = arr1[selectedRange[0]:selectedRange[1]]
    arr2_lag = lambda lag, step, sR_len: arr2[lag*step:lag*step+sR_len]
    # arr2_lag = lambda lag, step, sR_len: arr2[(lag*step)+startArr2_lag:(lag*step)+startArr2_lag+sR_len]

    selectedRange_len = selectedRange[1] - selectedRange[0]
    lagRange_len = int((len(arr2)-selectedRange_len)/step) + 1
    # lagRange_len = int(((len(arr2)-startArr2_lag)-selectedRange_len)/step) + 1   #При использовании startArr2_lag
        
    for lag in range(0, lagRange_len):
        LCC_value = LCC(arr1_fix, arr2_lag(lag, step, selectedRange_len))
        lag_time = ((lag * step)-selectedRange[0])
        if not isSeconds:
            lag_time = ((lag * step)-selectedRange[0]) / (samplerate) #в минуту
        write(pathfile, "{}\t{}".format(str(lag_time), str(LCC_value)))
    return True

#textfile
def write(pathfile,text):
        f = open(pathfile,'a')
        f.write(text+'\n')
        f.close()
def clearFile(pathfile):
        f = open(pathfile,'w')
        f.close()
def progress_bar(iteration, total, barLength):
        percent = int(round((iteration/total)*100))
        nb_bar_fill = int(round((barLength*percent)/100))
        bar_fill = '#'*nb_bar_fill
        bar_empty = ' ' * (barLength-nb_bar_fill)
        return ('\r [{0}] {1}%'.format(str(bar_fill+bar_empty),percent))
def consoleStatus(text):
        print('\r',end=text)
#
def read_wavfile(path):
    return wavfile.read(path)

#data:
# cl_array.arange(queue, 4000,dtype=numpy.float32)
# cl_array.to device(queue, numpy.array([1,2,3,4,5,6,6,8]).astype(numpy.float32))
# cl_array.to_device(queue, numpy.random.rand(quantity_el).astype(numpy.float32))
# Методы добавления данных в массив numpy.array
# numpy.append(arr, arr[0])
# numpy.concatenete(arr1,arr2)
# np.take(arr, range(0,len(arr)+1), mode='wrap')
quantity_el = 5000
#
arr1 = cl_array.to_device(queue, numpy.random.rand(quantity_el).astype(numpy.float32))  # Create a random pyopencl array
arr2 = arr1  # Create a random pyopencl array
#
def main(file1, file2, sR_0, sR_1, step, pathfile, samplerate=48000, isSeconds=False):
        arr1 = cl_array.to_device(queue, numpy.array(file1).astype(numpy.float32))
        arr2 = cl_array.to_device(queue, numpy.array(file2).astype(numpy.float32))
        if isSeconds:
            arr1 = cl_array.to_device(queue, compress_arr(arr1, samplerate))
            arr2 = cl_array.to_device(queue, compress_arr(arr2, samplerate))
        clearFile(pathfile)
        print('length_arr1: {} \nlength_arr2: {}'.format(len(arr1),len(arr2)))
                
        selectedRange = [sR_0, sR_1]
        
        start_timer = time.time()
        print('Timer: on')
        LCCRange_S(pathfile, samplerate, arr1, arr2, selectedRange, step, isSeconds=isSeconds)
        time_working = time.time()-start_timer
        print('\nTimer: stop; time: {} seconds'.format(round(time_working,3)))
# main()
