from scipy.io import wavfile
import pyopencl.array as cl_array
import numpy

from functools import reduce

import time


def read_wavfile(path):
    return wavfile.read(path)

def convertToNpArray(file):
    return numpy.ndarray(file).astype(numpy.float32)


def compress_list_length(list1, ordered_length):
    length_group = int(len(list1)/ordered_length)
    max_ordered_length = int(len(list1) / length_group)
    res_list = []
    for i in range(max_ordered_length):
        start_group_index = length_group * i
        finish_group_index = length_group * i + length_group
        part_list1 = list1[start_group_index: finish_group_index]
        abs_sum_numbers = reduce(lambda el1, el2: abs(el1)+abs(el2), part_list1)
        average_number = abs_sum_numbers/length_group
        res_list.append(average_number)
    return res_list

start_timer = time.time()
print('Timer: on')
print(len(compress_list_length(range(10000000), 500000)))
time_working = time.time()-start_timer
print('\nTimer: stop; time: {} seconds'.format(round(time_working,3)))