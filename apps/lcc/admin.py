from django.contrib import admin

from .models import DataFileModel

# Register your models here.

admin.site.register(DataFileModel)